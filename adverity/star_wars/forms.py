from django.forms import Form, ModelForm, MultipleChoiceField, CheckboxSelectMultiple

from adverity.star_wars.models import Person


class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = '__all__'


class PersonFieldsForm(Form):
    # TODO: Consider DRY refactor
    CHOICES = (
        ('name', 'Name'),
        ('height', 'Height'),
        ('mass', 'Mass'),
        ('hair_color', 'Hair color'),
        ('skin_color', 'Skin color'),
        ('eye_color', 'Eye color'),
        ('birth_year', 'Birth year'),
        ('gender', 'Gender'),
        ('homeworld', 'Homeworld'),
        ('date', 'Date'),
    )
    columns = MultipleChoiceField(
        widget=CheckboxSelectMultiple(attrs={'style': 'display: inline-block;'}), choices=CHOICES
    )
