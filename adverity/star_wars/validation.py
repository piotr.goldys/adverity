def clean_person(person: dict) -> dict:
    """
    Some info about my approach to validation/storing data:
    1) I left 'unknown' as valid eye_color to distinguish from possible case when somebody does not have eyes.
    2) I also left 'unknown' as valid planet name as it's own resource in SW-API.
    3) In case of multiple values for skin or hair color I just store them as strings, as I didn't find it important
        to store them separately in many-to-many fields.
    """
    return clean_birth_year(clean_skin_color(clean_hair_color(clean_floats(person))))


def clean_floats(person: dict) -> dict:
    FLOATS = ('mass', 'height')
    for field in FLOATS:
        try:
            person[field] = float(person[field].replace(',', ''))
        except ValueError:
            person[field] = None
    return person


def clean_hair_color(person: dict) -> dict:
    if person['hair_color'] == 'none':
        person['hair_color'] = None
    return person


def clean_skin_color(person: dict) -> dict:
    if person['skin_color'] in ['none', 'unknown']:
        person['skin_color'] = None
    return person


def clean_birth_year(person: dict) -> dict:
    if person['birth_year'] in ['none', 'unknown']:
        person['birth_year'] = None
    return person
