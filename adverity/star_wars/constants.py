SWAPI_URL = 'https://swapi.dev/api'
PEOPLE_URL = f'{SWAPI_URL}/people'
PLANETS_URL = f'{SWAPI_URL}/planets'
SWAPI_REDUNDANT_COLUMNS = ('films', 'species', 'vehicles', 'starships', 'created', 'edited', 'url')
