import requests


def fetch_results_by_page(url: str) -> list:
    """Fetch results from SW-API, one page at a time."""
    page_response = requests.get(url).json()
    try:
        yield page_response['results']

        while page_response['next']:
            page_response = requests.get(page_response['next']).json()
            yield page_response['results']
    except KeyError:
        return []


def get_resource_id_from_url(resource_url: str) -> int:
    return int(resource_url.split('/')[-2])
