from django.urls import path

from adverity.star_wars.views import (
    CollectionFetchView,
    CollectionListView,
    CollectionPeopleListView,
    CountCombinationsView,
    CollectionDownloadView,
)

urlpatterns = [
    path(
        'fetch-collection',
        CollectionFetchView.as_view(),
        name='collection-fetch',
    ),
    path(
        'collections',
        CollectionListView.as_view(),
        name='collection-list',
    ),
    path(
        'collections/<pk>/',
        CollectionPeopleListView.as_view(),
        name='collection-people',
    ),
    path(
        'collections/<pk>/combinations/',
        CountCombinationsView.as_view(),
        name='collection-combinations',
    ),
    path(
        'collections/<pk>/download/',
        CollectionDownloadView.as_view(),
        name='collection-download',
    ),
]
