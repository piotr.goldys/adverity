import csv
from datetime import date
from typing import Optional
from uuid import uuid4

import petl
from dateutil import parser
from django.conf import settings
from django.db import transaction
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views import View
from django.views.generic import ListView, FormView
from el_pagination.views import AjaxListView
from petl import Record, Table

from adverity.star_wars.constants import PLANETS_URL, SWAPI_REDUNDANT_COLUMNS, PEOPLE_URL
from adverity.star_wars.forms import PersonFieldsForm, PersonForm
from adverity.star_wars.models import Collection, Person, CollectionDownload
from adverity.star_wars.utils import fetch_results_by_page, get_resource_id_from_url
from adverity.star_wars.validation import clean_person


class CountCombinationsView(FormView):
    template_name = 'star_wars/combinations.html'
    form_class = PersonFieldsForm

    def get_initial(self):
        self.initial = dict(self.request.GET)
        return super().get_initial()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.GET:
            fields = self.request.GET.getlist('columns')
            context['headers'] = [*fields, 'count']
            context['people'] = (
                Person.objects.filter(collection_id=self.kwargs.get('pk'))
                .values_list(*fields)
                .annotate(count=Count('id'))
            )
        return context


class CollectionFetchView(View):
    planet_names = dict()

    def post(self, request, *args, **kwargs):
        self.fetch_all_planet_names()
        self.fetch_all_people()
        return redirect('collection-list')

    def fetch_all_planet_names(self):
        """
        Fetch and store planet names in dict to minimize amount of requests.
        We will use it to populate Person.homeworld.
        """
        for page_results in fetch_results_by_page(PLANETS_URL):
            for planet in page_results:
                self.planet_names[get_resource_id_from_url(planet['url'])] = planet['name']

    def fetch_all_people(self):
        results_gen = fetch_results_by_page(PEOPLE_URL)
        page_results = self.transform_results(next(results_gen))
        collection = self.save_to_db(page_results)
        for page in results_gen:
            page_results = self.transform_results(page)
            self.save_to_db(page_results, collection=collection)

    def get_planet_name(self, planet_url: str) -> str:
        return self.planet_names[get_resource_id_from_url(planet_url)]

    def transform_results(self, data: dict) -> Table:
        # Create new 'date' column
        table = petl.addfield(petl.fromdicts(data), 'date', self.get_date)
        # Drop redundant columns
        table = petl.cutout(table, *SWAPI_REDUNDANT_COLUMNS)
        # Populate 'homeworld' column with planet names
        return petl.convert(table, 'homeworld', self.get_planet_name)

    @staticmethod
    def get_date(person_record: Record) -> date:
        datetime_obj = parser.parse(person_record['edited'])
        return datetime_obj.date()

    @staticmethod
    def save_to_db(results: Table, collection: Optional[Collection] = None) -> Collection:
        with transaction.atomic():  # Save data do DB only if full fetch was successful
            if not collection:  # Create collection during first iteration.
                collection = Collection.objects.create()
            for person in petl.dicts(results):
                person['collection'] = collection.pk
                form = PersonForm(clean_person(person))
                if form.is_valid():
                    Person.objects.create(**form.cleaned_data)
        return collection


class CollectionDownloadView(View):
    def get(self, request, *args, **kwargs):
        collection_id = self.kwargs.get('pk')
        filename = f'collection-{uuid4().hex}.csv'

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename={filename}'

        writer = csv.writer(response)
        headers = [field.name for field in Person._meta.fields]

        # Exclude following fields from CSV export
        headers.remove('id')
        headers.remove('collection')

        writer.writerow(headers)

        people = Person.objects.filter(collection_id=collection_id).values_list(*headers)
        for person in people:
            writer.writerow(person)

        CollectionDownload.objects.create(collection_id=collection_id, filename=filename)

        return response


class CollectionListView(ListView):
    model = Collection
    queryset = Collection.objects.all()
    context_object_name = 'collection_list'


class CollectionPeopleListView(AjaxListView):
    model = Person
    context_object_name = 'people'
    page_template = 'star_wars/people_page.html'

    def get_queryset(self):
        return Person.objects.filter(collection_id=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['STATIC_URL'] = settings.STATIC_URL
        context['collection_id'] = self.kwargs.get('pk')
        return context
