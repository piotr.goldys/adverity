from django.db.models import (
    DateTimeField,
    CharField,
    IntegerField,
    FloatField,
    ForeignKey,
    CASCADE,
    DateField,
    SET_NULL,
)
from django.db.models import Model


class CreatedAtModel(Model):
    created_at = DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class Collection(CreatedAtModel):
    class Meta:
        ordering = ['-created_at']


class Person(Model):
    name = CharField(max_length=255)
    height = IntegerField(null=True, blank=True)
    mass = FloatField(null=True, blank=True)
    hair_color = CharField(max_length=31, null=True, blank=True)
    skin_color = CharField(max_length=63, null=True, blank=True)
    eye_color = CharField(max_length=63)
    birth_year = CharField(max_length=31, null=True, blank=True)
    gender = CharField(max_length=31)
    homeworld = CharField(max_length=63)
    date = DateField()
    collection = ForeignKey(Collection, on_delete=CASCADE, related_name='people')

    class Meta:
        ordering = ['id']


class CollectionDownload(CreatedAtModel):
    collection = ForeignKey(Collection, on_delete=SET_NULL, null=True)
    filename = CharField(max_length=63)
