Apply migrations before running.

All data collected from SW-API is stored in database for exploring previously collected datasets.
Since the data rarely changes in this particual API, we have a lot Person objects with the same data.  
We could think about optimizing the data model for space efficiency and reuse single Person for multiple datasets, if the data didn't change.

Other possible improvements:
- 'Load more' button appears also for last page, which is not necessary
- More exception handling/edge cases
- Timezone support
- Tests
